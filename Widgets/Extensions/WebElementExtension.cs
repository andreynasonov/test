﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Widgets.Extensions
{
    /// <summary>
    /// Расширение для работы с web элементами
    /// </summary>
    public static class WebElementExtension
    {
        /// <summary>
        /// Найти элемент, если он существует
        /// </summary>
        /// <param name="driver">Web Driver</param>
        /// <param name="by">Признак, по которому ищем</param>
        /// <returns>Элемент или null</returns>
        public static IWebElement GetWebElementIfExist(this IWebDriver driver, By by)
        {
            var isExist = driver.FindElements(by).Count > 0;
            if (isExist)
            {
                var myElement = driver.FindElement(by);
                return myElement;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Заполнение поля
        /// </summary>
        /// <param name="element">Элемент</param>
        /// <param name="message">Данные для заполнения</param>
        public static void Fill(this IWebElement element, string message)
        {
            element.Clear();
            element.SendKeys(message);
        }

        /// <summary>
        /// Установка элемента с нужным значением из списка
        /// </summary>
        /// <param name="elements">Список элементов</param>
        /// <param name="value">Значение нужного элемента</param>
        public static void SetWebElementByText(this IEnumerable<IWebElement> elements, string value)
        {
            var size = elements.Count();

            for (int i = 0; i < size; i++)
            {
                var element = elements.ElementAt(i);
                var elementValue = elements.ElementAt(i).GetAttribute("value");
                if (elementValue == value)
                {
                    elements.ElementAt(i).Click();
                    break;
                }
            }
        }

        /// <summary>
        /// Выбор значения из выпадающего списка
        /// </summary>
        /// <param name="element">Элемент</param>
        /// <param name="value">Значение</param>
        public static void SelectValueFromDropDown (this IWebElement element, string value)
        {
            var selectElement = new SelectElement(element);
            selectElement.SelectByText(value);
        }
    }
}
