﻿using Models;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Linq;
using Widgets.Extensions;

namespace Widgets.Widgets
{
    public class SegmentationWidget
    {
        private readonly IWebDriver _driver;

        #region Fields

        /// <summary>
        /// Поле КАСКО
        /// </summary>
        [FindsBy(How = How.Id, Using = "kasko")]
        private IWebElement KaskoCheckBox { get; set; }

        /// <summary>
        /// Поле марка ТС
        /// </summary>
        [FindsBy(How = How.Id, Using = "brand")]
        private IWebElement BrandSelectField { get; set; }

        /// <summary>
        /// Поле год выпуска
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//input[@name = 'yearRadio']")]
        private IList<IWebElement> YearRadioButton { get; set; }

        /// <summary>
        /// Поле модель для Mercedes
        /// </summary>
        [FindsBy(How = How.Id, Using = "mercedes")]
        private IWebElement MercedesModelSelectField { get; set; }

        /// <summary>
        /// Поле модель для BMW
        /// </summary>
        [FindsBy(How = How.Id, Using = "bmw")]
        private IWebElement BMWModelSelectField { get; set; }

        /// <summary>
        /// Поле тип кузова
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//input[@name = 'bodyTypeRadio']")]
        private IList<IWebElement> BodyTypeRadioButton { get; set; }

        /// <summary>
        /// Поле мощность двигателя
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//input[@name = 'enginePowerRadio']")]
        private IList<IWebElement> EnginePowerRadioButton { get; set; }

        /// <summary>
        /// Поле пробег
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//input[@name = 'mileageRadio']")]
        private IList<IWebElement> MileageRadioButton { get; set; }

        /// <summary>
        /// Поле наличие автозапуска
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//input[@name = 'autostartRadio']")]
        private IList<IWebElement> AutostartRadioButton { get; set; }

        /// <summary>
        /// Поле цена ТС
        /// </summary>
        [FindsBy(How = How.Id, Using = "priceValue")]
        private IWebElement PriceValueNumberField { get; set; }

        /// <summary>
        /// Поле регион использования
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//input[@name = 'regionOfUseRadio']")]
        private IList<IWebElement> RegionOfUseRadioButton { get; set; }

        /// <summary>
        /// Поле кредитное ли ТС
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//input[@name = 'creditRadio']")]
        private IList<IWebElement> CreditRadioButton { get; set; }

        /// <summary>
        /// Кнопка узнать стоимость
        /// </summary>
        [FindsBy(How = How.Id, Using = "findOutCost")]
        private IWebElement FindOutCostButton { get; set; }

        /// <summary>
        /// Поле фамилия водителя
        /// </summary>
        [FindsBy(How = How.XPath, Using = "(//input[@name = 'driverSurname'])[position() = last()-1]")]
        private IWebElement DriverSurnameTextField { get; set; }

        /// <summary>
        /// Поле имя водителя
        /// </summary>
        [FindsBy(How = How.XPath, Using = "(//input[@name = 'driverName'])[position() = last()-1]")]
        private IWebElement DriverNameTextField { get; set; }

        /// <summary>
        /// Поле отчество водителя
        /// </summary>
        [FindsBy(How = How.XPath, Using = "(//input[@name = 'driverPatronymic'])[position() = last()-1]")]
        private IWebElement DriverPatronymicTextField { get; set; }

        /// <summary>
        /// Поле пол водителя
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//input[@name = 'genderRadio']")]
        private IList<IWebElement> GenderRadioButton { get; set; }

        /// <summary>
        /// Поле возраст водителя
        /// </summary>
        [FindsBy(How = How.XPath, Using = "(//input[@name = 'age'])[position() = last()-1]")]
        private IWebElement AgeNumberField { get; set; }

        /// <summary>
        /// Поле стаж вождения водителя
        /// </summary>
        [FindsBy(How = How.XPath, Using = "(//input[@name = 'drivingExperience'])[position() = last()-1]")]
        private IWebElement DrivingExperienceNumberField { get; set; }

        /// <summary>
        /// Кнопка добавить водителя
        /// </summary>
        [FindsBy(How = How.Id, Using = "addDriver")]
        private IWebElement AddDriverButton { get; set; }

        #endregion

        public SegmentationWidget(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        /// <summary>
        /// Переход на страницу сегментации
        /// </summary>
        public void GoToSegmentation()
        {
            _driver.Navigate().GoToUrl("http://localhost:58022/Segmentation/index");
        }

        /// <summary>
        /// Заполнение выбор марки и года выпуска для КАСКО
        /// </summary>
        public void KaskoBrandYearDataFill(string brand, string year)
        {
            KaskoCheckBox.Click();
            BrandSelectField.SelectValueFromDropDown(brand);
            YearRadioButton.SetWebElementByText(year);
        }

        /// <summary>
        /// Заполнение данных для проверки предварительной стоимости
        /// </summary>
        public void SegmentationPageDataFill()
        {
            SegmentationPageData segmentationPageData = new SegmentationPageData();

            KaskoCheckBox.Click();
            BrandSelectField.SelectValueFromDropDown(segmentationPageData.Brand);
            YearRadioButton.SetWebElementByText(segmentationPageData.Year);
            MercedesModelSelectField.SelectValueFromDropDown(segmentationPageData.Model);
            BodyTypeRadioButton.SetWebElementByText(segmentationPageData.BodyType);
            EnginePowerRadioButton.SetWebElementByText(segmentationPageData.EnginePower);
            MileageRadioButton.SetWebElementByText(segmentationPageData.Mileage);
            AutostartRadioButton.SetWebElementByText(segmentationPageData.Autostart);
            PriceValueNumberField.Fill(segmentationPageData.Price);
            RegionOfUseRadioButton.SetWebElementByText(segmentationPageData.RegionOfUse);
            CreditRadioButton.SetWebElementByText(segmentationPageData.Credit);
            DriverInfoDataFill(segmentationPageData.Driver);
        }

        /// <summary>
        /// Добавление дополнительного водителя
        /// </summary>
        public void AddAdditionalDriver()
        {
            Driver driver = new Driver("Петров", "Петр", "Петрович", "М", "21", "3");

            AddDriverButton.Click();
            DriverInfoDataFill(driver);
        }

        /// <summary>
        /// Заполнение данных водителя
        /// </summary>
        public void DriverInfoDataFill(Driver driver)
        {
            DriverSurnameTextField.Fill(driver.Surname);
            DriverNameTextField.Fill(driver.Name);
            DriverPatronymicTextField.Fill(driver.Patronymic);
            GenderRadioButton.SetWebElementByText(driver.Gender);
            AgeNumberField.Fill(driver.Age);
            DrivingExperienceNumberField.Fill(driver.Experience);
        }

        /// <summary>
        /// Проверка доступности двух моделей ТС марки Mercedes 
        /// </summary>
        /// <param name="model1">Модель ТС</param>
        /// <param name="model2">Модель ТС</param>
        /// <returns>Доступны ли обе модели для выбора</returns>
        public bool IsAvailableMercedesModels(string model1, string model2)
        {
            bool isAvailable = false;

            if (MercedesModelSelectField.Displayed)
            {
                var dropdownMercedesModels = new SelectElement(MercedesModelSelectField);
                var isAvailableModel1 = dropdownMercedesModels.Options.Any(x => x.Text == model1);
                var isAvailableModel2 = dropdownMercedesModels.Options.Any(x => x.Text == model2);
                isAvailable = isAvailableModel1 && isAvailableModel2;
            }

            return isAvailable;
        }

        /// <summary>
        /// Переход в корзину
        /// </summary>
        public void FindOutCost()
        {
            FindOutCostButton.Click();
        }

        /// <summary>
        /// Сравнение цены ТС с входной
        /// </summary>
        /// <param name="carPrice">Проверяемая цена ТС</param>
        /// <returns>Признак совпадения</returns>
        public bool CompareCarPrice(string carPrice)
        {
            var price = PriceValueNumberField.GetAttribute("value");
            return price == carPrice;
        }
    }
}
