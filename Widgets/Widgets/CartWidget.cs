﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Widgets.Widgets
{
    public class CartWidget
    {
        private readonly IWebDriver _driver;

        #region Fields

        /// <summary>
        /// Кнопка назад
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//a[contains(text(), 'Назад')]")]
        private IWebElement BackLink { get; set; }

        #endregion

        public CartWidget(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        /// <summary>
        /// Удаление продукта 
        /// </summary>
        public void removeOfferFromCart()
        {
            var kaskoOffer = _driver.FindElement(By.Id("kasko"));
            kaskoOffer.Click();
        }

        /// <summary>
        /// Добавление апсейла в корзину
        /// </summary>
        public void addUpsaleToCart()
        {
            var osagoOffer = _driver.FindElement(By.Id("osago"));
            osagoOffer.Click();
        }

        /// <summary>
        /// Сравнение суммы в корзине с входной
        /// </summary>
        /// <param name="summ">Проверяемая сумма</param>
        /// <returns>Признак совпадения</returns>
        public bool compareCartSumm(string summ)
        {
            var cartSumm = _driver.FindElement(By.Id("summ")).Text.Split(':')[1];
            return cartSumm == summ;
        }

        /// <summary>
        /// Возврат к введённым данным
        /// </summary>
        public void GoToSegmentationPage()
        {
            BackLink.Click();
        }
    }
}
