﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Widgets.Widgets
{
    public class LoginWidget
    {
        private readonly IWebDriver _driver;

        #region Fields

        /// <summary>
        /// Поле email
        /// </summary>
        [FindsBy(How = How.Id, Using = "Email")]
        private IWebElement EmailTextField { get; set; }

        /// <summary>
        /// Поле пароля
        /// </summary>
        [FindsBy(How = How.Id, Using = "Password")]
        private IWebElement PasswordTextField { get; set; }

        /// <summary>
        /// Кнопка логина
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//input[@value = 'Log in']")]
        private IWebElement LoginButton { get; set; }

        #endregion

        public LoginWidget(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        /// <summary>
        /// Переход на страницу логина 
        /// </summary>
        public void GoToLogin()
        {
            _driver.Navigate().GoToUrl("http://localhost:58022/Account/Login");
        }

        /// <summary>
        /// Заполняем Email
        /// </summary>
        /// <param name="email">Email</param>
        public void SetEmail(string email)
        {
            EmailTextField.Clear();
            EmailTextField.SendKeys(email);
        }

        /// <summary>
        /// Заполняем пароль
        /// </summary>
        /// <param name="password">Пароль</param>
        public void SetPassword(string password)
        {
            PasswordTextField.Clear();
            PasswordTextField.SendKeys(password);
        }

        /// <summary>
        /// Нажатие кнопки Log In
        /// </summary>
        public void LoginButtonClick()
        {
            LoginButton.Click();
        }
    }
}
