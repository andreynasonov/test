﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Widgets.Widgets
{
    public class HomeWidget
    {
        private readonly IWebDriver _driver;

        #region Fields

        /// <summary>
        /// Log off
        /// </summary>
        [FindsBy(How = How.XPath, Using = "//a[contains(text(), 'Log off')]")]
        private IWebElement LogOffLink { get; set; }

        #endregion

        public HomeWidget(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        /// <summary>
        /// Переход на домашнюю страницу 
        /// </summary>
        public void GoToHomePage()
        {
            _driver.Navigate().GoToUrl("http://localhost:58022");
        }

        /// <summary>
        /// Выход из учётной записи
        /// </summary>
        public void LogOff()
        {
            LogOffLink.Click();
        }
    }
}
