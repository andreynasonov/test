﻿namespace Models
{
    public class Driver
    {
        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronymic { get; set; }

        /// <summary>
        /// Пол (М/Ж)
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Возраст
        /// </summary>
        public string Age { get; set; }

        /// <summary>
        /// Опыт вождения
        /// </summary>
        public string Experience { get; set; }

        public Driver()
        {
            Surname = "Иванов";
            Name = "Иван";
            Patronymic = "Иванович";
            Gender = "М";
            Age = "20";
            Experience = "2";
        }

        public Driver (string surname, string name, string patronymic,
                        string gender, string age, string experience)
        {
            Surname = surname;
            Name = name;
            Patronymic = patronymic;
            Gender = gender;
            Age = age;
            Experience = experience;
        }
    }
}
