﻿namespace Models
{
    public class SegmentationPageData
    {
        /// <summary>
        /// Марка
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// Год выпуска
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// Модель
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Тип кузова
        /// </summary>
        public string BodyType { get; set; }

        /// <summary>
        /// Мощность двигателя
        /// </summary>
        public string EnginePower { get; set; }

        /// <summary>
        /// Пробег (Есть/Нет)
        /// </summary>
        public string Mileage { get; set; }

        /// <summary>
        /// Наличие автозапуска (Есть/Нет)
        /// </summary>
        public string Autostart { get; set; }

        /// <summary>
        /// Цена
        /// </summary>
        public string Price { get; set; }

        /// <summary>
        /// Регион использования
        /// </summary>
        public string RegionOfUse { get; set; }

        /// <summary>
        /// Кредитное ли ТС (Да/Нет)
        /// </summary>
        public string Credit { get; set; }

        /// <summary>
        /// Водитель
        /// </summary>
        public Driver Driver { get; set; }

        public SegmentationPageData()
        {
            Brand = "Mercedes";
            Year = "2016";
            Model = "Mercedes model 1";
            BodyType = "Седан";
            EnginePower = "290";
            Mileage = "Есть";
            Autostart = "Нет";
            Price = "500000";
            RegionOfUse = "Волгоград";
            Credit = "Нет";
            Driver = new Driver();
        }
    }
}
