﻿using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Widgets.Widgets;

namespace test.Steps
{
    [Binding]
    public class СвязанныеСправочникиFeatureSteps : BaseSteps
    {
        private IWebDriver _driver;
        private SegmentationWidget segmentationPage;

        public СвязанныеСправочникиFeatureSteps(IWebDriver driver)
        {
            _driver = driver;
            segmentationPage = new SegmentationWidget(_driver);
        }

        [Given(@"Пользователь приступил к расчёту стоимости продукта для ТС")]
        public void GivenПользовательПриступилКРасчётуСтоимостиПродуктаДляТС()
        {   
            segmentationPage.GoToSegmentation();
        }
        
        [When(@"Пользователь выбрал марку ТС (.*) и (.*) год")]
        public void WhenПользовательВыбралМаркуТСИГод(string brand, string year)
        {
            segmentationPage.KaskoBrandYearDataFill(brand, year);
        }
        
        [Then(@"Пользователь может выбрать модели ТС марки Mercedes: (.*), (.*)")]
        public void ThenПользовательМожетВыбратьМоделиТСМаркиMercedesMercedesModelMercedesModel(string model1, string model2)
        {
            var isAvailable = segmentationPage.IsAvailableMercedesModels(model1, model2);
            Execute(() => Assert.IsTrue(isAvailable), _driver);
        }
    }
}
