﻿using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Widgets.Widgets;

namespace test.Steps
{
    [Binding]
    public class ФункционалСтраницыСегментацииSteps : BaseSteps
    {
        private IWebDriver _driver;
        private SegmentationWidget segmentationPage;

        public ФункционалСтраницыСегментацииSteps(IWebDriver driver)
        {
            _driver = driver;
            segmentationPage = new SegmentationWidget(_driver);
        }

        [When(@"Пользователь добавил дополнительного водителя")]
        public void WhenПользовательДобавилДополнительногоВодителя()
        {
            segmentationPage.AddAdditionalDriver();
        }
    }
}
