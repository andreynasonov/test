﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using test.Steps;
using Widgets.Extensions;
using Widgets.Widgets;

namespace test
{
    [Binding]
    public class АвторизацияПользователяSteps : BaseSteps
    {
        private IWebDriver _driver;
        private LoginWidget loginPage;
        private HomeWidget homePage;

        public АвторизацияПользователяSteps(IWebDriver driver)
        {
            _driver = driver;
            loginPage = new LoginWidget(_driver);
            homePage = new HomeWidget(_driver);
        }

        [Given(@"Пользователь заходит на страницу логина")]
        public void GivenПользовательЗаходитНаСтраницуЛогина()
        {
            loginPage.GoToLogin();
        }
        
        [Given(@"Пользователь вводит свой Email")]
        public void GivenПользовательВводитСвойEmail()
        {
            loginPage.SetEmail("test@mail.ru");
        }
        
        [Given(@"Пользователь вводит свой пароль")]
        public void GivenПользовательВводитСвойПароль()
        {
            loginPage.SetPassword("123123aA!");
        }
        
        [Given(@"Пользователь вводит неправильный пароль")]
        public void GivenПользовательВводитНеправильныйПароль()
        {
            loginPage.SetPassword("123");
        }
        
        [When(@"Пользователь кликает на кнопку LogIn")]
        public void WhenПользовательКликаетНаКнопкуLogIn()
        {
            loginPage.LoginButtonClick();
        }
        
        [Then(@"Пользователь видит сообщение об ошибке")]
        public void ThenПользовательВидитСообщениеОбОшибке()
        {
            var error = _driver.GetWebElementIfExist(By.XPath("//li[contains(text(),'Invalid login attempt.')]"));
            if (error == null)
            {
                Execute(() => Assert.Fail("Сообщения об ошибке нет."), _driver);
            }
        }

        [Then(@"Пользователь видит домашнюю страницу с виджетом логаута")]
        public void ThenПользовательВидитДомашнююСтраницуСВиджетомЛогаута()
        {
            var logOffWidget = _driver.GetWebElementIfExist(By.XPath("//a[contains(text(), 'Log off')]"));
            if (logOffWidget == null)
            {
                Execute(() => Assert.Fail("Логаут виджет не найден."), _driver);
            }
        }

        [Given(@"Пользователь залогинен")]
        public void GivenПользовательЗалогинен()
        {
            homePage.GoToHomePage();

            var logOffWidget = _driver.GetWebElementIfExist(By.XPath("//a[contains(text(), 'Log off')]"));
            if (logOffWidget == null)
            {
                loginPage = new LoginWidget(_driver);
                loginPage.GoToLogin();
                loginPage.SetEmail("test@mail.ru");
                loginPage.SetPassword("123123aA!");
                loginPage.LoginButtonClick();
            }
        }

        [When(@"Пользователь разлогинивается")]
        public void WhenПользовательРазлогинивается()
        {
            homePage.LogOff();
        }

        [Then(@"Пользователь видит домашнюю страницу с виджетом логина")]
        public void ThenПользовательВидитДомашнююСтраницуСВиджетомЛогина()
        {
            var logInWidget = _driver.GetWebElementIfExist(By.XPath("//a[contains(text(), 'Log in')]"));
            if (logInWidget == null)
            {
                Execute(() => Assert.Fail("Логин виджет не найден."), _driver);
            }
        }
    }
}
