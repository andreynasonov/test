﻿using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Widgets.Extensions;
using Widgets.Widgets;

namespace test.Steps
{
    [Binding]
    public class КорзинаSteps : BaseSteps
    {
        private IWebDriver _driver;
        private SegmentationWidget segmentationPage;
        private CartWidget cartPage;

        public КорзинаSteps(IWebDriver driver)
        {
            _driver = driver;
            segmentationPage = new SegmentationWidget(_driver);
            cartPage = new CartWidget(_driver);
        }

        [When(@"Пользователь проверил предварительную стоимость")]
        public void WhenПользовательПроверилПредварительнуюСтоимость()
        { 
            segmentationPage.FindOutCost();
        }
        
        [Then(@"Пользователь увидел в корзине оффер (.*)")]
        public void ThenПользовательУвиделВКорзинеОффер(string offerName)
        {
            var offer = _driver.GetWebElementIfExist(By.XPath("//div[@id='cart']/p[contains(text(), '" + offerName + "')]"));
            if (offer == null)
            {
                Execute(() => Assert.Fail("Оффер " + offerName + " в корзине не найден."), _driver);
            }
        }

        [Then(@"Пользователь увидел апсейл оффер (.*)")]
        public void ThenПользовательУвиделАпсейлОффер(string offerName)
        {
            var offer = _driver.GetWebElementIfExist(By.XPath("//p[@id='osagoName' and contains(text(), '" + offerName + "')]"));
            if (offer == null)
            {
                Execute(() => Assert.Fail("Оффер " + offerName + " в апсейле не найден."), _driver);
            }
        }

        [When(@"Пользователь удалил продукт из корзины")]
        public void WhenПользовательУдалилПродуктИзКорзины()
        {
            cartPage.removeOfferFromCart();
        }

        [Then(@"Пользователь увидел сумму в корзине, равную (.*)")]
        public void ThenПользовательУвиделСуммуВКорзинеРавную(string cartSumm)
        {
            Execute(() => Assert.IsTrue(cartPage.compareCartSumm(cartSumm)), _driver);
        }

        [When(@"Пользователь добавил апсейл в корзину")]
        public void WhenПользовательДобавилАпсейлВКорзину()
        {
            cartPage.addUpsaleToCart();
        }

        [When(@"Пользователь решил отредактировать введённые данные")]
        public void WhenПользовательРешилОтредактироватьВведённыеДанные()
        {
            cartPage.GoToSegmentationPage();
        }

        [Then(@"Пользователь видит, что он ввёл стоимость ТС, равную (.*)")]
        public void ThenПользовательВидитЧтоОнВвёлСтоимостьТСРавную(string carPrice)
        {
            Execute(() => Assert.IsTrue(segmentationPage.CompareCarPrice(carPrice)), _driver);
        }

        [When(@"Пользователь корректно заполнил данные о себе и объекте страхования для расчёта предварительной стоимости")]
        public void WhenПользовательКорректноЗаполнилДанныеОСебеИОбъектеСтрахованияДляРасчётаПредварительнойСтоимости()
        {
            segmentationPage.SegmentationPageDataFill();
        }

    }
}
