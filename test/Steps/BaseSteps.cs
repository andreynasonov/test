﻿using OpenQA.Selenium;
using System;

namespace test.Steps
{
    public class BaseSteps
    {
        Hooks hooks = new Hooks();

        public void Execute(Action action, IWebDriver driver)
        {
            try
            {
                action();
            }
            catch
            {
                hooks.TakeScreenshotOnException(driver);
                throw;
            }
        }
    }
}
