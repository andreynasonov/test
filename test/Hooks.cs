﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.Events;
using System;
using System.IO;
using System.Linq;
using TechTalk.SpecFlow;

namespace test.Steps
{
    [Binding]
    public class Hooks
    {
        private readonly IObjectContainer _objectContainer;
        private IWebDriver _driver;

        public Hooks()
        {
        }

        public Hooks(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;
        }

        [BeforeScenario]
        public void Initialize()
        {
            var options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            _driver = new EventFiringWebDriver(new ChromeDriver(options));
            ((EventFiringWebDriver)_driver).ExceptionThrown += TakeScreenshotOnException;
            _objectContainer.RegisterInstanceAs(_driver);
        }

        [AfterScenario]
        public void CleanUp()
        {
            _driver.Quit();
        }

        public void TakeScreenshotOnException(object sender, WebDriverExceptionEventArgs e = null)
        {
            var screenshot = ((ITakesScreenshot)sender).GetScreenshot();
            // Поиск названия теста, вызвавшего исключение
            var stackTraceMethod = Environment.StackTrace.Split(' ')
                                        .Where(x => x.Contains("test.Features")).ToArray()[0]
                                        .Split('.');
            var testName = stackTraceMethod[stackTraceMethod.Length - 1];

            var path = System.Reflection.Assembly.GetExecutingAssembly().Location
                        + "\\..\\..\\..\\..\\ErrorScreenshots\\" + testName 
                        + DateTime.Now.ToString("-yyyy-MM-dd_HH-mm-ss") + ".jpg";

            Directory.CreateDirectory(Path.GetDirectoryName(path));
            screenshot.SaveAsFile(path, ScreenshotImageFormat.Jpeg);
        }
    }
}
